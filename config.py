from dotenv import load_dotenv
import os

load_dotenv()

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    DATABASE_FILE = os.environ["DATABASE_FILE"]
    SECRET_KEY = os.environ["SECRET_KEY"]
    DEBUG = False
    TESTING = False

class DevelopmentConfig(Config):
    DEBUG = True

class TestingConfig(Config):
    pass

class ProductionConfig(Config):
    pass

config = {
    "development": DevelopmentConfig,
    "testing": TestingConfig,
    "production": ProductionConfig
}[os.environ.get("FLASK_ENV", "development")]
