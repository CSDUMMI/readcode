{ pkgs ? import <nixpkgs> {}}:
  pkgs.mkShellNoCC {
     packages = with pkgs; [
       (python311.withPackages (pyPkgs: [
	   pyPkgs.flask
	   pyPkgs.peewee
	   pyPkgs.peewee-migrate
       ]))
     ];
}
