from read.auth import auth_blueprint
from read import db
from flask import render_template, redirect, url_for, current_app, flash
from read.auth.forms import (
    RegisterForm,
    LoginForm,
)
from flask_login import login_user, login_required, logout_user, current_user

@auth_blueprint.route("/register", methods=["GET","POST"])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        player = Player.create(
            username=form.username.data,
            password=form.password.data,
        )
        login_user(player)
        flash("Account created. You have been logged in", "success")
        return redirect(url_for("main.start"))
    return render_template("auth/register.html", form=form)


@auth_blueprint.route("/login", methods=["GET","POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        player = db.Player.get_or_none(db.Player.username==form.username.data)
        if player and player.verify_password(form.password.data):
            login_user(player)
            flash("Logged in", "success")
            return redirect(url_for("main.start"))
        else:
            flash("Check credentials and try again", "danger")
    return render_template("auth/login.html", form=form)


