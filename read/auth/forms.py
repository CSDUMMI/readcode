from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Email, EqualTo, ValidationError, Regexp
from read import db

class RegisterForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired(), Regexp("^[a-zA-Z0-9_]*$",flags=0, message="Username can only contain alphanumerical characters")])
    password = PasswordField("Password", validators=[DataRequired(), EqualTo("confirm_password", message="Passwords must match")])
    confirm_password = PasswordField("Confirm Password", validators=[DataRequired()])
    submit = SubmitField("Register")

    def validate_username(self, field):
        if db.Player.select().where(db.Player.username == field.data.lower()).exists():
            raise ValidationError("Username already in use")

class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    passsword = PasswordField("Password", validators=[DataRequired()])
    submit = SubmitField("Login")

    def validate_username(self, field):
        if not db.Player.select().where(db.Player.username == field.data.lower).exists():
            raise ValidationError("Username does not exist")

