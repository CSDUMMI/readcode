from config import config
from flask_login import UserMixin, AnonymousUserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
import peewee as pw

database = pw.SqliteDatabase(config.DATABASE_FILE)

class BaseModel(pw.Model):
    class Meta:
        database = database

        
class Player(BaseModel, UserMixin):
    """Main user account
    """
    username = pw.TextField()
    password_hash = pw.CharField()

    @property
    def password(self):
        raise AttributeError("Password is not readable")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)


    def verify_password(self, password):
        return check_password(self.password_hash, password)

    
class CodeSnippet(BaseModel):
    """A code snippet excerpted from a FOSS project
    """
    title = pw.TextField()
    code = pw.TextField()
    source = pw.TextField()
    license = pw.TextField()
    language = pw.CharField()
    author = pw.ForeignKeyField(Player, backref="code_snippets")
    accepted = pw.BooleanField()

    
class Explanation(BaseModel):
    """An explanation, opinion or statement on the code snippet 
    """
    text = pw.TextField()
    snippet = pw.ForeignKeyField(CodeSnippet, backref="explanations")
    author = pw.ForeignKeyField(Player, backref="explanations")
    created_at = pw.DateTimeField(default=datetime.now)
    updated_at = pw.DateTimeField(default=datetime.now)

    def edit(self, text):
        """Forces edits to update the updated_at field as well
        """
        self.text = text
        self.updated_at = datetime.now()
        self.save()
        

class Review(BaseModel):    
    """A review of the explanation to provide a second opinion on them and improve everyone's understanding
    """
    text = pw.TextField()
    explanation = pw.ForeignKeyField(Explanation, backref="reviews")
    author = pw.ForeignKeyField(Player, backref="reviews")
    created_at = pw.DateTimeField(default=datetime.now)
    updated_at = pw.DateTimeField(default=datetime.now)

    def edit(self, text):
        """Forces edits to update the updated_at field as well 
        """
        self.text = text
        self.updated_at = datetime.now()
        self.save()
