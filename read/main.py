from flask import Flask, request, jsonify
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect
import read.db as db
from config import config

def create_app():
    app = Flask(__name__)
    app.config.from_object(config)

    login_manager = LoginManager()
    login_manager.init_app(app)

    csrf = CSRFProtect(app)
    csrf.init_app(app)
    
    @login_manager.user_loader
    def load_player(player_id):
        return db.Player.get_by_id(player_id)

    from read.auth import auth_blueprint

    app.register_blueprint(auth_blueprint, url_prefix="/auth")
    
    return app

