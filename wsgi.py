from read import create_app, db
import os

app = create_app()

@app.shell_context_processor
def make_shell_context():
    return dict(db=db)

